document.addEventListener('DOMContentLoaded', function(){
	var slider = $('#slider'),
		item = slider.children('.slide'),
		step = 0,
		canvas = document.getElementById('slider').getElementsByTagName('canvas'),
		video = document.getElementById('slider').getElementsByTagName('video'),
		vidContent = [],
		wW = $(window).width(),
		wH = $(window).height();

	init();


	function init(){
		item.eq(step).addClass('active');

		item.each(function(){
			var $this = $(this);

			var v = document.getElementById('v' + ($this.index() + 1));
			var canvas = document.getElementById('c' + ($this.index() + 1));
			var context = canvas.getContext('2d');
			var back = document.createElement('canvas');
			var backcontext = back.getContext('2d');
			var clip = [v.clientWidth, v.clientWidth, v.clientHeight];
			var fclip = [v.clientWidth*0.6, v.clientWidth*0.4, v.clientHeight];

			vidContent.push(false);

			v.addEventListener('play', function(){
				canvas.width = wW;
				canvas.height = wH;
				back.width = wW;
				back.height = wH;
				requestAnimationFrame(animate);

				function animate(time){
					if((clip[0]>fclip[0] || clip[1]>fclip[1]) && vidContent[$this.index()]){

						clip[0] = clip[0]>=fclip[0] ? clip[0]-20 : fclip[0];
						clip[1] = clip[1]>=fclip[1] ? clip[1]-32 : fclip[1];
						draw(v,context,backcontext,wW,wH, clip);
					} else {
						draw(v,context,backcontext,wW,wH, clip);
					}
					setTimeout(function(){
						requestAnimationFrame(animate);
					}, 33)
				}
			},false);
			v.play();
			v.currentTime = 5;
			draw(v,context,backcontext,wW,wH, clip);
			v.pause();
		});

		vidContent[0] = true;
		video[0].play();
	}


	/*var v = document.getElementById('v1');
	var canvas = document.getElementById('c1');
	var context = canvas.getContext('2d');
	var back = document.createElement('canvas');
	var backcontext = back.getContext('2d');
	var clip = [v.clientWidth, v.clientWidth, v.clientHeight];
	var fclip = [v.clientWidth*0.7, v.clientWidth*0.5, v.clientHeight];

	var cw,ch;



	v.addEventListener('play', function(){
		cw = v.clientWidth;
		ch = v.clientHeight;
		canvas.width = cw;
		canvas.height = ch;
		back.width = cw;
		back.height = ch;
		requestAnimationFrame(animate);

		function animate(time){
			if(clip[0]>fclip[0] || clip[1]>fclip[1]){

				clip[0] = clip[0]>=fclip[0] ? clip[0]-10 : fclip[0];
				clip[1] = clip[1]>=fclip[1] ? clip[1]-16 : fclip[1];
				draw(v,context,backcontext,cw,ch, clip);
			} else {
				draw(v,context,backcontext,cw,ch, clip);
			}
			setTimeout(function(){
				requestAnimationFrame(animate);
			}, 33)
		}
	},false);

	v.play();*/


},false);

function draw(v,c,bc,w,h, clip) {
	//if(v.paused || v.ended) return false;
	// First, draw it into the backing canvas

		bc.clearRect(0,0,w,h);
		bc.save();
		bc.beginPath();
		bc.moveTo(0, 0);
		bc.lineTo(clip[0], 0);
		bc.lineWidth = 1;
		bc.lineTo(clip[1], h);
		bc.lineWidth = 1;
		bc.lineTo(0, h);
		bc.lineWidth = 1;

		bc.clip();

	bc.drawImage(v,0,0,w,h);
	// Grab the pixel data from the backing canvas
	var idata = bc.getImageData(0,0,w,h);
	var data = idata.data;
	// Loop through the pixels, turning them grayscale
	for(var i = 0; i < data.length; i+=4) {
		var r = data[i];
		var g = data[i+1];
		var b = data[i+2];
	}
	idata.data = data;
	// Draw the pixels onto the visible canvas
	c.putImageData(idata,0,0);
	bc.restore();
	// Start over!
	//setTimeout(function(){ draw(v,c,bc,w,h); }, 0);
}